package Tree;

public class TraverseTest {
		public static void main(String[] args) 
		{

			Node a = new Node("A");
			Node b = new Node("B");
			Node c = new Node("C");
			Node d = new Node("D");
			Node e = new Node("E");
			Node f = new Node("F");
			Node g = new Node("G");
			Node h = new Node("H");
			Node i = new Node("I");
			
			ReportConsole rcon = new ReportConsole();
			
			f.setLeft(b);
			f.setRight(g);

			b.setLeft(a);
			b.setRight(d);

			d.setLeft(c);
			d.setRight(e);

			g.setRight(i);

			i.setLeft(h);
			
			PreOrderTraversal preOrder = new PreOrderTraversal();
			InOrderTraversal inOrder = new InOrderTraversal();
			PostOrderTraversal postOrder = new PostOrderTraversal();

			System.out.print("PreOrderTranversal : ");
			rcon.display(f, preOrder);
			System.out.println("");
			System.out.print("InOrderTranversal : ");
			rcon.display(f, inOrder);
			System.out.println("");
			System.out.print("PostOrderTranversal : ");
			rcon.display(f, postOrder);
		}
	}