package Tree;

public class Node {
	private String value;
	private Node left;
	private Node right;
	
	public Node(String v) {
		this.value = v;
	}

	public String getValue(){
		return value;
	}
	
	public Node getLeft(){
		return left;
	}
	
	public Node getRight(){
		return right;
	}
	
	public void setLeft(Node leftNode)
	{
		this.left = leftNode;
	}
	
	public void setRight(Node rightNode)
	{
		this.right = rightNode;
	}
	
	public String toString(){
		return getValue();
	}

}
