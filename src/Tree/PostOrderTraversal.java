package Tree;

import java.awt.List;
import java.util.ArrayList;

public class PostOrderTraversal implements Traversal {
	@Override
	public ArrayList<Node> traverse(Node  node) {
		ArrayList<Node> nodeList = new ArrayList<Node>();
		
		if(node.getLeft() != null){
			ArrayList<Node> nLeft = traverse(node.getLeft());
			
			for (int i = 0;i < nLeft.size();i++){
				nodeList.add(nLeft.get(i));
			}
		}
			
		if(node.getRight() != null){
			ArrayList<Node> nRight = traverse(node.getRight());
				
			for(int i = 0;i < nRight.size();i++){
				nodeList.add(nRight.get(i));
			}
		}
		
		nodeList.add(node);
		
		return nodeList;
	}
}
