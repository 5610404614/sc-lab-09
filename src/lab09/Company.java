package lab09;

public class Company implements Taxable{
	
	private String companyName ;
	private double income;
	private double outcome;
	private double profit;


	public Company(String name, double in, double out) {
		this.companyName = name;
		this.income = in;
		this.outcome = out;
		this.profit = this.income-this.outcome;
		
	}


	@Override
	public double getTax() {
		
		return (income-outcome)*(0.3);
	}
	
	public String getNameCompany(){
		return companyName;
	}
	
	public double getIncome(){
		return income;
	}
	
	public double getOutcome(){
		return outcome;
	}
	
	public double getProfit(){
		return profit;
	}
	
	public String toString() {
		return "Company< name = "+this.companyName+", income = "+this.income+", outcome = "+this.outcome +">";
	}


	public String getName() {
		return companyName;
	}
}