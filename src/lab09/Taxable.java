package lab09;

public interface Taxable {
	double getTax();
}
