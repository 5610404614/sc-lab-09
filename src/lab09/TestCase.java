package lab09;

import java.util.ArrayList;
import java.util.Collections;

public class TestCase {

	public static void main(String[] args) {
			
			//  Person 
		
			System.out.println("---------- Person ----------");		
			ArrayList<Person> person = new ArrayList<Person>();
			Person person1 = new Person("Yomost", 200000);
			Person person2 = new Person("Mochi", 500000);
			Person person3 = new Person("Je'taime", 800000);
			person.add(person1);
			person.add(person2);
			person.add(person3);
			
			System.out.println("<< Before sort >>");		
			for (Person psBf : person) {			
			System.out.println(psBf.getName() + ":  " +psBf.getIncome() + " baht.");
			}
			
			Collections.sort(person);
			System.out.println("<< After sort >>");
			for (Person psAt : person) {
			System.out.println(psAt.getName() + ":  " + psAt.getIncome() + " baht.");		
			}
	
			//  Product 
			System.out.println("\n---------- Product ----------");		
			ArrayList<Product> product = new ArrayList<Product>();
			Product product1 = new Product("Computer",5000);
			Product product2 = new Product("Phone",1000);
			Product product3 = new Product("DVD",3500);
			product.add(product1);
			product.add(product2);
			product.add(product3);
			
			System.out.println("<< Before sort price >>");		
			for (Product pdBf : product) {			
				System.out.println(pdBf.getName() + ":  " + pdBf.getPrice() + " baht.");
			}
		
			Collections.sort(product);
			System.out.println("<< After sort price >>");
			for (Product pdAt : product) {
			System.out.println(pdAt.getName() + ":  " +pdAt.getPrice() + " baht.");		
			}
			
			// Company's Income
			System.out.println("\n---------- Company's Income ----------");		
			ArrayList<Company> income = new ArrayList<Company>();
			Company income1 = new Company("A", 100000, 20000);
			Company income2 = new Company("B", 20000, 5000);
			Company income3 = new Company("C", 300000, 100000);
			income.add(income1);
			income.add(income2);
			income.add(income3);
			
			System.out.println("<< Before sort with Income >>");		
			for (Company cBf : income) {			
			System.out.println(cBf.getName() + ":  " +cBf.getIncome());
			}
			
			Collections.sort(income, new EarningComparator());
			System.out.println("<< After sort with Income >>");
			for (Company cAt : income) {
			System.out.println(cAt.getName() + ":  " + cAt.getIncome());		
			}

			// --------- Company's Outcome ----------
			System.out.println("\n---------- Company's Outcome ----------");		
			ArrayList<Company> outcome = new ArrayList<Company>();
			Company outcome1 = new Company("A", 100000, 20000);
			Company outcome2 = new Company("B", 20000, 5000);
			Company outcome3 = new Company("C", 300000, 100000);
			outcome.add(outcome1);
			outcome.add(outcome2);
			outcome.add(outcome3);
			
			System.out.println("<<Before sort with Outcome >>");		
			for (Company cBf: outcome) {			
			System.out.println(cBf.getName() + ":  " + cBf.getOutcome());
			}
			
			Collections.sort(outcome, new ExpenseComparator());
			System.out.println("<< After sort with Outcome >>");
			for (Company cAt : outcome) {
			System.out.println(cAt.getName() + ":  " +cAt.getOutcome());		
			}
			
			// --------- Company's Profit ----------
			System.out.println("\n---------- Company's Profit ----------");		
			ArrayList<Company> profit = new ArrayList<Company>();
			Company profit1 = new Company("A", 100000, 50000);
			Company profit2 = new Company("B", 10000, 5000);
			Company profit3 = new Company("C", 200000, 100000);
			profit.add(profit1);
			profit.add(profit2);
			profit.add(profit3);
			
			System.out.println("<< Before sort with Profit >>");		
			for (Company pBefore : profit) {			
			System.out.println(pBefore.getProfit());
			}
			
			Collections.sort(profit, new ProfitComparator());
			System.out.println("<< After sort with Profit >>");
			for (Company pAfter : profit) {
			System.out.println(pAfter.getProfit());		
			}
			
			// --------- All Sort ---------- 
	
			ArrayList<Taxable> all = new ArrayList<Taxable>();
			Person personTax = new Person("Aimmy",100000);
			Company companyTax= new Company("A",1000000,800000);
			Product productTax = new Product("Charger",500);
			all.add(personTax);
			all.add(productTax);
			all.add(companyTax);
			
			System.out.println("<< Before sort with All Tax >>");		
			for (Taxable taxBf : all) {			
			System.out.println(taxBf.getTax());
			}
			
			Collections.sort(all,new TaxCalculator());
			System.out.println("<< After sort with All Tax >>");
			for (Taxable taxAt : all) {
			System.out.println(taxAt.getTax());		
			}
	}
}
