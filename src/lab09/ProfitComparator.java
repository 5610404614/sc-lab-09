package lab09;

import java.util.Comparator;

public class ProfitComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		double cp1 = ((Company) o1).getProfit();
		double cp2 = ((Company) o2).getProfit();
		if (cp1 > cp2) {
			return 1;
		}
		if (cp1 < cp2) {
			return -1;
		}
		return 0;
	}
}
