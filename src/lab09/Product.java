package lab09;

public class Product implements Taxable, Comparable<Product> {
	private String productName;
	private double price;

	public Product(String name, double price) {
		this.productName = name;
		this.price = price;
	}

	@Override
	public double getTax() {

		return (price * 7) / 100;
	}

	public String toString() {
		return "Product <name = " + this.productName + ", Price = "
				+ this.price + ">";
	}
	
	public String getName(){
		return productName;
	}

	@Override
	public int compareTo(Product other) {

		if (this.price < other.price) {
			return -1;
		}
		if (this.price > other.price) {
			return 1;
		}
		return 0;
	}

	public double getPrice() {
		return price;
	}

}
