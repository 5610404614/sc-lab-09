package lab09;

import java.util.Comparator;

public class ExpenseComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		double cp1 = ((Company)o1).getOutcome();
		double cp2 = ((Company)o2).getOutcome();
		if (cp1 > cp2){
			return 1;
		}
		if (cp1 < cp2){
			return -1;
		}
		return 0;
	}

}
