package lab09;

import java.util.Comparator;

public class EarningComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		double earn1 = ((Company)o1).getIncome();
		double earn2 = ((Company)o2).getIncome();
		if (earn1 > earn2) return 1;
		if (earn1 < earn2) return -1;
		return 0;
	}
}
