package lab09;

import java.util.ArrayList;

public class Person implements Taxable, Comparable<Person> {

	private ArrayList<Person> person_data;
	private String name;
	private double income;

	public Person(String name, double income) {
		this.name = name;
		this.income = income;
	}

	@Override
	public double getTax() {
		if (income <= 300000 && income >= 0) {

			return income * (0.05);
		}

		return 15000 + (0.1) * (income - 300000);
	}

	public double getIncome() {

		return income;
	}

	public String toString() {
		return "Person< name = " + this.name + ", income = " + this.income
				+ ">";
	}

	@Override
	public int compareTo(Person other) {
		if (this.income < other.income) {
			return -1;
		}
		if (this.income > other.income) {
			return 1;
		}
		return 0;
	}

	public String getName() {
		return name;
	}
}
